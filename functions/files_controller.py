import os


def get_last_edited(folder):
    files = os.listdir('sources/' + folder + '/')

    files = [os.path.join('sources/' + folder + '/', file) for file in files]
    files = [file for file in files if os.path.isfile(file)]

    return max(files, key=os.path.getmtime)