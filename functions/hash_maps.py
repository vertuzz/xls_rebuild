import openpyxl
from functions import files_controller


def cat_map():  # Функция возвращает словарь с формате "назание категории":{"main":id,"parent":id}
    # то есть номер категори и номер ее материнской категории
    # Открытие файла с категориями
    cat_wb = openpyxl.load_workbook(filename=files_controller.get_last_edited('categories'))
    # Выбор активного листа
    ws_cat = cat_wb.active

    data_cat = tuple(ws_cat.iter_rows())[1:]  # Получение всех строк кроме заголовка

    map_cat_ids = {}

    for row in data_cat:
        map_cat_ids[row[2].value] = {"main": row[0].value, "parent": row[1].value}
    # row[2] - название категории, row[0] - id основной, row[1] - материнской
    return map_cat_ids
