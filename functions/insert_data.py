import openpyxl, re
from functions import files_controller

from functions import hash_maps

# Открытие входного и выходного excel файлов
input_wb = openpyxl.load_workbook(filename=files_controller.get_last_edited('new_goods'))
output_wb = openpyxl.load_workbook(filename=files_controller.get_last_edited('existing_goods'))
# Выбор листов из файлов
ws_input = input_wb.active
ws_output = output_wb.get_sheet_by_name("Products")
ws_output_attr = output_wb.get_sheet_by_name("ProductAttributes")

data = tuple(ws_input.iter_rows())[1:]  # Все строки из входного файла
cat_map = hash_maps.cat_map()


def insert_all_products():  # Вставка всех строк их входного файла в лист Products
    num_of_row = ws_output.max_row + 1  # Начальная строка для вставки
    last_id = ws_output.cell(row=ws_output.max_row, column=1).value  # Id последнего товара
    for row in data:
        if row[0].value is None:
            break
        insert_row_in_products_sheet(num_of_row, last_id, row)
        # if row[4].value is None and row[7].value is None and row[8].value is None and row[9].value is None:
        #     num_of_row += 1  # Инкремент строки
        #     last_id += 1  # Инкремент id товара
        #     continue
        # if row[7].value is None and row[8].value is None and row[9].value is None:
        #     insert_attr_from_one_cell(last_id, row[4].value)
        # else:
        # insert_attributes(last_id, row)
        num_of_row += 1  # Инкремент строки
        last_id += 1  # Инкремент id товара

    output_wb.save('forInsert.xlsx')


def insert_row_in_products_sheet(number_of_row, last_product_id, data_row):
    product_id = last_product_id + 1

    # Дефолтные значения
    coll_id = 1
    for cell in tuple(ws_output.rows)[1]:  # Берутся из 1 строки шаблонного файла
        ws_output.cell(row=number_of_row, column=coll_id, value=cell.value)
        coll_id += 1

    # Значения из входного файла
    ws_output.cell('A' + str(number_of_row), value=product_id)  # Id товара
    ws_output.cell('B' + str(number_of_row), value=data_row[2].value)  # Наименование товара
    ws_output.cell('C' + str(number_of_row), value=get_cat_ids(data_row[1].value, 1))  # Категории
    ws_output.cell('D' + str(number_of_row), value=get_cat_ids(data_row[1].value, 0))  # Категория (основная)
    ws_output.cell('M' + str(number_of_row), value=product_id)  # Модель
    ws_output.cell('O' + str(number_of_row), value=img_path(data_row[5].value))  # Путь к картинке
    ws_output.cell('Q' + str(number_of_row), value=online_price_change(data_row[4].value))  # Цена
    ws_output.cell('AE' + str(number_of_row), value=str(data_row[6].value).replace('\n', '<br>') + "<br>Особенности<br>" +
                   str(data_row[7].value).replace('\n', '<br>') + "<br>Описание<br>" + str(data_row[8].value).replace('\n', '<br>'))  # Описание


def insert_attributes(last_product_id, data_row):  # Вставка атрибутов на старницу атрибутов
    attr_names = tuple(ws_input.iter_rows())[0][5:13]  # Список атрибутов в добавляемом файле
    product_id = last_product_id + 1  # ID продукта
    row = ws_output_attr.max_row + 1  # Строка для вставки

    attr_id = 5  # Начальный номер столбцов атрибутов
    for attribute in attr_names:
        if not data_row[attr_id].value:
            attr_id += 1
            continue
        ws_output_attr.cell('A' + str(row), value=product_id)
        ws_output_attr.cell('B' + str(row), value=ws_output_attr.cell('B2').value)
        ws_output_attr.cell('C' + str(row), value=attribute.value)
        ws_output_attr.cell('D' + str(row), value=data_row[attr_id].value)
        attr_id += 1
        row += 1


def insert_attr_from_one_cell(last_product_id, cell):
    product_id = last_product_id + 1  # ID продукта
    row = ws_output_attr.max_row + 1  # Строка для вставки

    list_of_params = []
    for item in cell.split('/'):
        list_of_params.append(item.strip())

    params_dict = {}
    for param in list_of_params:
        if ' Вт' in param:
            params_dict['Мощность, Вт'] = param
        elif ' лм' in param:
            params_dict['Световой поток, Лм'] = param
        elif 'IP' in param:
            params_dict['IP Защита'] = param
        elif ' K' in param:
            params_dict['Цветовая температура, К'] = param

    for key in sorted(params_dict.keys()):
        ws_output_attr.cell('A' + str(row), value=product_id)
        ws_output_attr.cell('B' + str(row), value=ws_output_attr.cell('B2').value)
        ws_output_attr.cell('C' + str(row), value=key)
        ws_output_attr.cell('D' + str(row), value=params_dict[key])
        row += 1


def img_path(name):

    pre_path = 'catalog/my_img/for_goods/shtok/'
    final_path = pre_path + str(name)
    return final_path


def online_price_change(row_price):
    if not row_price:
        return str(0)
    good_price = float(str(row_price).replace(',', '.').strip().replace(' ', ''))
    untaxed_price = round((good_price / 1.18) * 1.2, 2)
    return untaxed_price


def get_cat_ids(cat_name, parents=0):
    ids_dict = cat_map

    if cat_name in ids_dict:
        if parents == 0:
            return str(ids_dict[cat_name]['main'])
        elif parents == 1:
            cat_keys = str(ids_dict[cat_name]['parent']) + ',' + str(ids_dict[cat_name]['main'])
            return cat_keys
    else:
        print('Wrong category name')
