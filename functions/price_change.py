import openpyxl
from functions import files_controller


def price_change():
    wb = openpyxl.load_workbook(filename=files_controller.get_last_edited('existing_goods'))
    ws = wb.get_sheet_by_name("Products")

    data = tuple(ws.iter_rows())[1:]  # Все строки из входного файла
    number_of_row = 2
    for row in data:
        old_price = row[16].value
        new_price = round(old_price / 1.18)
        ws.cell('Q'+str(number_of_row), value=new_price)
        number_of_row += 1
    wb.save('forInsert.xlsx')
